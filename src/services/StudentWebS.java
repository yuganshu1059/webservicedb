package services;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import beans.Student;
import beans.StudentService;
import beans.StudentServiceImpl;

@Path("get")
@Consumes("application/json")
public class StudentWebS {

	StudentService sp = new StudentServiceImpl();

	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path("student/{roll}")
	public Student getStudent(@PathParam("roll") int roll) {
		return sp.getStudent(roll);

	}

	@POST
	@Path(value = "students")
	@Produces("application/json")
	public String getAllstudents(String obj) {
		JsonParser parser = new JsonParser();
		JsonObject object = parser.parse(obj).getAsJsonObject();
	
		String account=object.get("accountNo").getAsString();
		System.out.println("comming here");
		Map<String, Object> map = new LinkedHashMap<>();
		if(account.equalsIgnoreCase("123456789"))		
		{
			map.put("status", "true");
			map.put("Money Transfer", "5000");
			map.put("Tranfer In ", "Sumit");
			map.put("message", "Successfully Transfer");

		} else {
			map.put("status", "false");
			map.put("message", "Account Does not exist");
		}
		
		String json = new Gson().toJson(map);
		return json;

	}

}
