package appinit;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


@ApplicationPath("/ws")
public class ApplicationInit extends Application {

	@Override
	public Map<String, Object> getProperties() {
	
		HashMap<String, Object> hs = new HashMap<>();
		
		hs.put("jersey.config.server.provider.packages", "services");
		
		return hs;     
	}
}
