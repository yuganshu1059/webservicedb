package beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StudentServiceImpl implements StudentService {

	@Override
	public Student getStudent(int roll) {

		Student s = new Student();

		try {

			Connection con = this.getConnection();

			if (con != null) {
				PreparedStatement ps = con.prepareStatement("select * from student where roll=?");
				ps.setInt(1, roll);

				ResultSet rs = ps.executeQuery();

				if (rs.next()) {
					s = new Student();

					s.setRoll(roll);
					s.setName(rs.getString("name"));
					s.setGrade(rs.getString("grade"));
					s.setCls(rs.getInt("cls"));

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println(s.getName());
		System.out.println(s.getRoll());
		System.out.println(s.getCls());
		System.out.println(s.getGrade());

		return s;

	}

	@Override
	public List<Student> getAllStudents() {
		List<Student> students = null;

		Connection c = this.getConnection();
		if (c != null) {
			students = new ArrayList<>();
			try {
				ResultSet rs = c.createStatement().executeQuery("select * from student");
				while (rs.next()) {
					Student student = new Student();
					student.setRoll(rs.getInt("roll"));
					student.setName(rs.getString("name"));
					student.setGrade(rs.getString("grade"));
					student.setCls(rs.getInt("cls"));
					students.add(student);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

		return students;

	}

}
