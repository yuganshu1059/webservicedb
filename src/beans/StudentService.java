package beans;

import java.sql.Connection;
import java.util.List;

public interface StudentService {

	abstract Student getStudent(int rollNo);

	List<Student> getAllStudents();

	default Connection getConnection() {
		Connection con = null;
		try {
			con = DbProvider.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return con;
	}

}
